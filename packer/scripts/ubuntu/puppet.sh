#!/bin/sh -eux

apt-get install -y python-software-properties

cd /tmp
wget http://apt.puppetlabs.com/puppetlabs-release-trusty.deb
sudo dpkg -i puppetlabs-release-trusty.deb

apt-get update
apt-get install -y puppet-common

# Create empty hiera.yaml config file to prevent error message during puppet apply
touch /etc/puppet/hiera.yaml

# Remove depricated option from puppet config file
sed -i -e '/templatedir=/d' /etc/puppet/puppet.conf
