class bower
{
    exec { 'npm-install-bower': 
		command => 'npm install -g bower@1.6.8',
        unless => 'which bower',
        environment => ['HOME=/home/vagrant'],
        require => [Package['nodejs'], Package['npm'], Package['git']]
    }
}