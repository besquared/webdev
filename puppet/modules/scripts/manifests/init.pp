class scripts
{

    file { '/home/vagrant/scripts':
		source => 'puppet:///modules/scripts/scripts',
        recurse => true,
        owner => vagrant,
        group => vagrant,
    }

    exec { 'make-scripts-executable':
		command => 'chmod +x /home/vagrant/scripts/remove_dotunderscore',
        require => File['/home/vagrant/scripts']
    }

    file { '/home/vagrant/.bash_profile':
		ensure => file,
        owner => vagrant,
        group => vagrant,
        notify => File_line['cd-to-www-dir']
    }

    file_line { 'load-bashrc':
		path => '/home/vagrant/.bash_profile',
        line => '[ -f ~/.bashrc ] && . ~/.bashrc',
        require => File['/home/vagrant/.bash_profile']
    }

    file_line { 'cd-to-www-dir': 
		path => '/home/vagrant/.bash_profile',
        line => '[ -d /var/www ] && cd /var/www',
        require => File_Line['load-bashrc']
    }
}