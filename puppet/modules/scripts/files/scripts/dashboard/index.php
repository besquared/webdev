<?php


$dir = new DirectoryIterator('/home/vagrant/projects');
$i   = 1;
$sites = array();

foreach ($dir as $fileinfo)
{

    if ($fileinfo->isDir() && !$fileinfo->isDot())
    {
		$sites[] = (object) array(
			'name'    => $fileinfo->getFilename(),
			'docroot' => $fileinfo->getFilename());
    }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="B. van Wetten (BeSquared)">
    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="assets/images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png">
	<link rel="manifest" href="assets/images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="assets/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

    <title>WebDev | Dashboard</title>

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for the dashboard -->
    <link href="assets/css/dashboard.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/javascripts/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img class="logo" src="assets/images/logo_2_small.png" alt="WebDev Box">WebDev box</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="https://gitlab.com/besquared/webdev">Contribute on GitLab</a></li>
            <li><a href="#" target="_blank">Docs</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Dashboard</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li role="presentation" class="dropdown-header">Tools</li>
            <li><a href="http://phpmyadmin.webdev.box:8080">PHPMyAdmin</a></li>
            <li><a href="/mailcatcher">MailCatcher</a></li>
            <?php if (function_exists('apc_cache_info') && @apc_cache_info('opcode')): ?>
                <li><a href="/apc">APC dashboard</a></li>
            <?php endif; ?>
            <?php if (function_exists('zray_disable')): ?>
                <li><a href="http://joomla.box:8080/ZendServer">Z-Ray</a></li>
            <?php endif; ?>
            <?php if (extension_loaded('xdebug')): ?>
                <li><a href="http://webgrind.joomla.box">Webgrind</a></li>
            <?php endif; ?>
          </ul>
          <ul class="nav nav-sidebar">
            <li role="presentation" class="dropdown-header">System</li>
              <li><a href="/phpinfo">PHPInfo</a></li>
              <li><a href="/pimpmylog">Log Files</a></li>
              <li><a href="/filebrowser">File Browser</a></li>
              <li><a href="/terminal">Terminal</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div class="table-responsive">
            <table class="table table-striped table-dashboard">
              <thead>
                <tr>
                  <th width="10">#</th>
                  <th>Sites Running On This Box</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              <?php
              $i = 1;
              foreach ($sites as $site): ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td>
                    <a target="_blank" href="/<?php echo $site->docroot . '/administrator/'; ?>">
                      <?php echo $site->name ?></a>                    
                  </td> 
                  <td>
                    <div class="btn-group">
                      <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Options <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                          <li><a href="/<?php echo $site->docroot; ?>" target="_blank">Site</a></li>
                          <li><a href="/<?php echo $site->docroot . '/administrator/'; ?>" target="_blank">Administrator</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php
                $i++;
                endforeach;
              ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="assets/javascripts/bootstrap.js"></script>
  </body>
</html>
