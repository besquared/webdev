####################
# Global variables #
####################
$box_version 		= '0.0.3'
$servername  		= 'webdev.box'
$serverip			= '127.0.0.1'
$server_http_port	= '80'
$server_https_port	= '443'

#######################
# Set puppet defaults #
#######################
Exec
{
    path => ['/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/', '/usr/local/bin/'], timeout => 900
}
File
{
    owner => 0, group => 0, mode => 0644
}

############################
# Set the correct timezone #
############################
class { 'timezone':
	region   	=> 'Etc',
	locality 	=> 'UTC',
	hwutc 		=> true
}

####################################
# Set the hostname and box version #
####################################
system::hostname { $servername:
	ip => $serverip
}

file
{
    '/etc/profile.d/webdev-box.sh': ensure => present,
    content => "export WEBDEV_BOX=${::box_version}\n",
}

#############################################
# copy dot files and other config/ini files #
#############################################
file { '/home/vagrant/.bash_aliases':
	ensure => 'present',
    owner => vagrant,
    group => vagrant,
    source => 'puppet:///modules/dot/bash_aliases',
}

#################################################
# Install Package manager & standalone packages #
#################################################
class { 'apt': 
	update => {
		frequency => 'daily',
	},
	require => Package['software-properties-common']
}

apt::ppa { 'ppa:ondrej/php5-5.6': }

Class['::apt::update'] -> Package <| title != 'python-software-properties' and title != 'software-properties-common' |>

package {
	[
		'build-essential',
		'software-properties-common',
		'ruby-dev',
		'php5-dev',
		'nano',
		'vim',
		'curl',
		'git',
		'unzip',
		'htop'		
	]:
	ensure		=> 'installed',
	provider	=> 'apt'
}

package {
	[
		'sass'	
	]:
	ensure		=> 'installed',
	provider	=> 'gem'
}


######################
# Install NTP Client #
######################
class { '::ntp':
	servers   => ['0.pool.ntp.org', '1.pool.ntp.org', '2.pool.ntp.org', '3.pool.ntp.org'],
	restrict  => [
		'default ignore',
		'-6 default ignore',
		'127.0.0.1',
		'-6 ::1',
		'0.pool.ntp.org nomodify notrap nopeer noquery',
		'1.pool.ntp.org nomodify notrap nopeer noquery',
		'2.pool.ntp.org nomodify notrap nopeer noquery',
		'3.pool.ntp.org nomodify notrap nopeer noquery',
	],
}

###################
# Install scripts #
###################
class { 'scripts': }

##################
# Install NodeJS #
##################
class { 'nodejs': }

package { [
	'waitpid',
	'websocket',
	'grunt-cli'
] :
	ensure   => 'present',
	provider => 'npm',
	require	 => Class['nodejs']
}

###############
# Install PHP #
###############
class { ['php::fpm', 'php::cli', 'php::extension::apc']: 
	require => Package['php5-dev']
}

class { 'php::extension::gd': }
class { 'php::extension::imagick': }

################################################
# Install Composer (PHP Dependency management) #
################################################
class { 'composer':
	command_name => 'composer',
	target_dir   => '/usr/bin',
	require => Class['php::fpm'],
}

#################################################
# Install Bower (A package manager for the web) #
#################################################
class { 'bower': }

################
# Install MOTD #
################
file { '/etc/update-motd.d/999-webdev':
  ensure => 'present',
  mode   => 'u+rwx,go+rx',
  source => 'puppet:///modules/motd/webdev',
}

file { ['/etc/update-motd.d/10-help-text', '/etc/update-motd.d/91-release-upgrade', '/etc/update-motd.d/50-landscape-sysinfo', '/etc/update-motd.d/51-cloudguest', '/etc/update-motd.d/90-updates-available', '/etc/update-motd.d/98-cloudguest']:
  ensure => absent
}

##########################################
# Install wetty (Web Based SSH terminal) #
##########################################
class { 'wetty' : }

#####################################################
# Install cloudcommander (Web Based file management #
#####################################################
class { 'cloudcommander' : }

#######################
# Install mailcatcher #
#######################
class { 'mailcatcher' : }

#######################
# Install pimpmylog #
#######################
class { 'pimpmylog' :
	require => [Class['apache']]
}

#################
# Install MySQL #
#################
class { '::mysql::server':
	root_password           => 'mysql',
	remove_default_accounts => true,
	override_options        => $override_options
}

######################
# Install PHPMyAdmin #
######################
class { 'phpmyadmin':
	require => [Class['apache'], Class['::mysql::server'], Class['php::fpm']] 
}

#############################
# Install Apache web server #
#############################
class { 'apache':
	servername 		=> "${::servername}",
	default_vhost 	=> false,
}

#apache::listen { '0.0.0.0:80': }
#apache::listen { '[::]:80': }

class { 'apache::mod::rewrite': }
class { 'apache::mod::ssl': }
class { 'apache::mod::headers': }
class { 'apache::mod::actions': }

apache::fastcgi::server { 'php':
	socket	   => '/var/run/php5-fpm.sock',
	timeout    => 15,
	flush      => false,
	faux_path  => '/var/www/php.fcgi',
	fcgi_alias => '/php.fcgi',
	file_type  => 'application/x-httpd-php'
}

apache::vhost { "${servername}":
	serveradmin => 'webmaster@localhost',
    serveraliases => [
		'localhost',
		'webdev',
		'webdev.box'
	],
	
	aliases => [
	{
		alias	=> '/phpinfo',
		path	=> '/home/vagrant/scripts/phpinfo.php',
	},
	{
		alias	=> '/dashboard',
		path	=> '/home/vagrant/scripts/dashboard',
	},
	{
		alias	=> '/apc',
		path	=> '/home/vagrant/scripts/apc-dashboard.php',
	},
	{
		alias	=> '/pimpmylog',
		path	=> '/usr/share/pimpmylog/vendor/potsky/pimp-my-log',
	},
	{
		alias	=> '/projects',
		path	=> '/home/vagrant/projects',
	}],
	
	rewrites => [
    {
		comment      => 'redirect to dashboard',
		rewrite_cond => ['%{REQUEST_URI} ^/$'],
		rewrite_rule => ['(.*) /dashboard/ [R=301]'],
    },
	],

	redirect_source => ['/terminal','/mailcatcher','/filebrowser'],
	redirect_dest   => ["http://${::servername}:3000","http://${::servername}:1080","http://${::servername}:8001"],
	
    priority => '00',
	port => "${server_http_port}",
    docroot => '/var/www',
    directories => [
    {
        'path' => '/var/www',
        'allow_override' => 'All',
		'require' => 'all granted',
        'options' => ['FollowSymLinks', 'MultiViews'],
    },
	{
        'path' => '/home/vagrant/scripts',
        'allow_override' => 'All',
		'require' => 'all granted',
    },
	{
        'path' => '/home/vagrant/projects',
        'allow_override' => 'All',
		'require' => 'all granted',
    },	
	{
        'path' => '/usr/share/pimpmylog',
        'allow_override' => 'All',
		'require' => 'all granted',        
    }],
	
	custom_fragment => '  AddType application/x-httpd-php .php',
	
	error_log_file => "${logroot}${servername}-error_log",
    access_log_file => "${logroot}${servername}-access_log"
}

apache::vhost { 'phpmyadmin':
	servername   	=> 'phpmyadmin',
	serveradmin 	=> 'webmaster@localhost',
    serveraliases 	=> [
		'phpmyadmin.webdev.box'
	],
	docroot			=> '/usr/share/phpmyadmin',
	port        	=> "8080",
	priority 		=> '10',
  
	directories => [
    {
        'path' 				=> '/usr/share/phpmyadmin',
        'allow_override' 	=> 'All',
		'require' 			=> 'all granted',
        'options' 			=> ['FollowSymLinks', 'MultiViews'],
    }, ],
  
  	error_log_file 	=> "${logroot}${servername}-error_log",
    access_log_file => "${logroot}${servername}-access_log",

	custom_fragment => '  AddType application/x-httpd-php .php',
		
	#require 		=> [Class['phpmyadmin']] 
}

#####################################
# System user & group configuration #
#####################################
group
{
	'puppet': ensure => present
}

user
{
    'vagrant': ensure => present
}